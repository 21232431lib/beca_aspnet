﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CLIENTE.Models
{
    [MetadataType(typeof(Cliente.MetaData))]
    public partial class Cliente
    {
        sealed class MetaData
        {
            /* Indicamos que es primary key*/
            [Key]
            public int Cliid;

            /*Indicamos que es un campo requerido y desplegamos mensaje*/
            [Required(ErrorMessage = "Ingresa el nombre del cliente")]
            public string Cliname;


            /*Indicamos que es un campo requerido, validamos formato y desplegamos mensaje*/
            [Required]
            [EmailAddress(ErrorMessage = "Ingresa email valido")]
            public string Email;


            [Required]
            [Range(0, 1000000, ErrorMessage = "Valor invalido ")]
            public Nullable<int> Credit;

            [Required]
            [Range(18, 60, ErrorMessage = "Edad entre 18 y 60")]
            public Nullable<int> Age;

        }
    }
}
 