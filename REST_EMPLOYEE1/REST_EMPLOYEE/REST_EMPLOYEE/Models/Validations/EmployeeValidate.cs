﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_EMPLOYEE.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData {
            [Key]
            public int Empid;

            [Required(ErrorMessage = "Ingresa el nombre de empleado")]
            public string Empname;

            [Required]
            [EmailAddress(ErrorMessage = "Ingresa email valido")]
            public string Email;

            [Required]
            [Range(20, 50,ErrorMessage = "Edad entre 20 y 50")]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa salario")]
            public Nullable<int> Salary;
        }
    }
}