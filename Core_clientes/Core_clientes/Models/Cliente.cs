﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_clientes.Models
{
    public class Cliente
    {
            [Key]
            public int Cliid { get; set; }

            [Required(ErrorMessage = "Enter the client name")]
            [Display(Name = "Client Name")]
            public string Cliname { get; set; }

            [Required(ErrorMessage = "Enter the client email")]
            [Display(Name = "Email")]
            [EmailAddress(ErrorMessage = "Enter a valid email address")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Enter the client credit")]
            [Display(Name = "Credit")]
            [Range(0, 1000000)]
            public int Credit { get; set; }

            [Required(ErrorMessage = "Enter the client age")]
            [Display(Name = "Age")]
            [Range(20, 50)]
            public int Age { get; set; }
        }

}
