﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Core_clientes.Models;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;

namespace Core_clientes.Controllers
{
    public class ClienteController : Controller
    {
        /*Varibale para accder a base de datos*/
        private readonly ApplicationDbContext _db;

        /*Constructor*/
        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Cliente.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string CliSearch)
        {
            ViewData["GetClienteDetail"] = CliSearch; 
             var Cliquery = from x in _db.Cliente select x;
            if (!String.IsNullOrEmpty(CliSearch))
            {
                Cliquery = Cliquery.Where(x => x.Cliname.Contains(CliSearch) ||
                x.Email.Contains(CliSearch));
            }
            return View(await Cliquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Cliente nCli)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nCli);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Cliente oldCli)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldCli);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) 
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }
       
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getCliDetail = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(getCliDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}