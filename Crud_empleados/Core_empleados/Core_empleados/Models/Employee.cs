﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Core_empleados.Models
{
    public class Employee
    {
        [Key]
        public int Empid { get; set; }

        [Required(ErrorMessage = "Enter the employee name")]
        [Display(Name = "Employee Name")]
        public string Empname { get; set; }

        [Required(ErrorMessage = "Enter the employee email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Enter a valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter the employee age")]
        [Display(Name = "Age")]
        [Range(20, 50)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Enter the employee salary")]
        [Display(Name = "Salary")]
        public int Salary { get; set; }
    }
}

