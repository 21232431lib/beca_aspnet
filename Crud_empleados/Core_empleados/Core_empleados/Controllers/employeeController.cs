﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core_empleados.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_empleados.Controllers
{
    public class employeeController : Controller
    {
        /*Una variable para acceder a la base de datos*/
        private readonly ApplicationDbContext _db;

        /*Constructor*/
        public employeeController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Employee.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string empSearch) 
        {
            ViewData["GetEmployeesDetails"] = empSearch;
            var empquery = from x in _db.Employee select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x=>x.Empname.Contains(empSearch)||
                x.Email.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Employee nEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Employee oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpdetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpdetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
